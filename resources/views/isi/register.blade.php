<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
            @csrf
            <div>
                <label>First Name</label><br><br>
                <input type="text" name="nama"><br><br>
                <label>Last Name:</label><br><br>
                <input type="text" name="nama1">
            </div>

            <div>
                <br>
                <label>Gender:</label><br><br>
                <input type="radio" name="gender" value="M"> Male<br>
                <input type="radio" name="gender" value="F"> Female<br>
                <input type="radio" name="gender" value="O"> Other<br>
            </div>

            <div>
                <br>
                <label>Nationality</label><br>
                <select>
                    <option value="IDN">Indonesia</option>
                    <option value="AMK">Amerika</option>
                    <option value="IGN">Inggris</option>
                </select>
            </div>

            <div>
                <br>
                <label>Language Spoken</label><br>
                <input type="checkbox" name="language" value="bahasa">Bahasa Indonesia<br>
                <input type="checkbox" name="language" value="english">English<br>
                <input type="checkbox" name="language" value="other">Other<br>
            </div>

            <div>
                <br>
                <label>Bio</label><br>
                <textarea rows="10" cols="30"></textarea>
            </div>

            <br>
            <input type="submit" value="sign up">
        </form>

</body>
</html>